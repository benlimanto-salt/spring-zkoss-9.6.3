package id.salt.malang.fif.zulController;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Textbox;

import id.salt.malang.fif.controllers.HomeController;
import id.salt.malang.fif.models.Product;
import id.salt.malang.fif.service.ProductService;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class CreateProductController extends SelectorComposer<Component>  {
	private static final long serialVersionUID = 1L;
	
	@Wire
	public Textbox productName;
	
	@Wire
	public Textbox productPrice;
	
	@Wire
	public Textbox productNotes;
	
	@WireVariable("productService")
	private ProductService service;
	
	@Listen("onClick = #btnSubmit; onOK = window")
	public void Submit()
	{
//		MapService();
		
		var p = new Product();
		p.setName(productName.getValue());
		p.setNotes(productNotes.getValue());
		p.setPricing(Integer.parseInt(productPrice.getValue()));
		
		service.create(p);
		
		alert("New Product Recorded");
		
		Executions.sendRedirect("/product");
	}
	
	public void MapService()
	{
		service = HomeController.productService;
	}
}
