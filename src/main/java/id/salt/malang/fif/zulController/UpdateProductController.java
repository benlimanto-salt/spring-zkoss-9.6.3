package id.salt.malang.fif.zulController;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Button;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;

import id.salt.malang.fif.controllers.HomeController;
import id.salt.malang.fif.models.Product;
import id.salt.malang.fif.service.ProductService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class UpdateProductController extends SelectorComposer<Component>  {
	private static final long serialVersionUID = 1L;
	
	@Wire
	public Textbox productName;
	
	@Wire
	public Textbox productPrice;
	
	@Wire
	public Textbox productNotes;
	
	@Wire
	public Button btnSubmit;
	
	@WireVariable("productService")
	private ProductService service;
	private Product p;
	private int id = -1;
	
    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        MapData();
    }
	
	@Listen("onClick = #btnSubmit; onOK = window")
	public void Submit()
	{
		
		p.setName(productName.getValue());
		p.setNotes(productNotes.getValue());
		p.setPricing(Integer.parseInt(productPrice.getValue()));
		
		service.update(p);
		
		Messagebox.show("Success save product", new Messagebox.Button[] {Messagebox.Button.OK},
 		new EventListener<Messagebox.ClickEvent>() { //optional
 			public void onEvent(Messagebox.ClickEvent event) {
 				Executions.sendRedirect("/product");
 			}
 		});

//		var exec = (Execution) Executions.getCurrent();
//		var httpResponse = (HttpServletResponse) exec.getNativeResponse();
		
	}
	
	public void MapData()
	{

		Execution exec = Executions.getCurrent();
		HttpServletRequest request = (HttpServletRequest) exec.getNativeRequest();
		
		Object o = request.getSession().getAttribute("id");
		
		if (o != null) {
			String idSession = String.valueOf(o);
			request.getSession().removeAttribute("id");
			String idUrl = idSession;
			
			try {
				id = Integer.parseInt(idUrl);
			}
			catch(Exception e)
			{
				Messagebox.show("Its not int : " + idUrl, new org.zkoss.zul.Messagebox.Button[] { org.zkoss.zul.Messagebox.Button.OK }, null);
			}
		}
		else 
			id = Integer.parseInt(Executions.getCurrent().getParameter("id"));
		
		p = service.get(id);
		
		if (p == null) 
		{
			Executions.sendRedirect("/product");
			return;
		}
		
		this.productName.setValue(p.getName());
		this.productPrice.setValue(String.valueOf(p.getPricing()));
		this.productNotes.setValue(p.getNotes());
		
		this.btnSubmit.setDisabled(false);
	}
}
 