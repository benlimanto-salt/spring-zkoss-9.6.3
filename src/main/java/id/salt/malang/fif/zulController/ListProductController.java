package id.salt.malang.fif.zulController;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.MouseEvent;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Button;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;

import id.salt.malang.fif.controllers.HomeController;
import id.salt.malang.fif.models.Product;
import id.salt.malang.fif.service.ProductService;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class ListProductController extends SelectorComposer<Component>  {
	private static final long serialVersionUID = 1L;
	
	@Wire
	public Listbox listProduct;
	
	@Wire
	public Button btnDelete;
	
	private ListModelList<Product> dataModel = new ListModelList<Product>();
	
	@WireVariable("productService")
	ProductService service;
	
    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
//        Selectors.wireVariables(comp, this, _resolvers);
        
        listProduct.setModel(dataModel);
//        MapService();
        MapData();
    }
    
	public void MapService()
	{
		service = HomeController.productService;
	}
	
	public void MapData()
	{
		dataModel.clear();
		var products = service.getList(null, null);
		
		products.forEach(s -> dataModel.add(s));
	}
	
	@Listen("onClick = #btnDelete; onOK = window")
	public void DeleteAction()
	{
		if (listProduct.getSelectedIndex() < 0) return; // do nothing
		Product p = dataModel.get(listProduct.getSelectedIndex());
		
		MapService();
		service.delete(p);
		MapData();
		alert("Delete Successful");
	}
	
	@Listen("onSelect = #listProduct")
	public void OnSelect()
	{
		this.btnDelete.setDisabled(false);
	}
	
	@Listen("onClick = listcell > button.btnEdit; onOK = window")
	public void goToEdit(MouseEvent event)
	{
		var p = (Button) event.getTarget();
//		var l = p.getParent().getParent().getFirstChild();
//		Listcell c = (Listcell) l;
		String id = p.getAutag();
		Executions.sendRedirect("/product/edit?id="+id);
	}
	
	@Listen("onClick = listcell > button.btnEditParam; onOK = window")
	public void goToEditParam(MouseEvent event)
	{
		var p = event.getTarget();
//		var l = p.getParent().getParent().getFirstChild();
//		Listcell c = (Listcell) l;
		String id = p.getAutag();
		Executions.sendRedirect("/product/edit/"+id);
	}
}
