package id.salt.malang.fif.zulController;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Textbox;

import id.salt.malang.fif.controllers.HomeController;
import id.salt.malang.fif.service.ProductService;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class IndexController extends SelectorComposer<Component> {
    private static final long serialVersionUID = 1L;

    @WireVariable("productService")
    public ProductService service;
    
    @Wire
    public Textbox name;
    
    @Listen("onClick = #btnSubmit")
    public void changeName()
    {
//    	MapService(); // Ughh.. this is workaround for now, THIS IS UNTESTABLE CODE! MANIAC!
    	var first = service.get(1);
    	
    	name.setValue(first.getName());
    }
    
    public void MapService()
    {
    	service = HomeController.productService;
    }
}
