package id.salt.malang.fif.zulController;

import org.springframework.security.core.context.SecurityContextHolder;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Label;

public class LoggedUserPart extends SelectorComposer<Component> {
	
	@Wire
	public Label loggedUserLbl;
	
	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		var auth = SecurityContextHolder.getContext().getAuthentication();
		loggedUserLbl.setValue(auth.getName());
	}
}
