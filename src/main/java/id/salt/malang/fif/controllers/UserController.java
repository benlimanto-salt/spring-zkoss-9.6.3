package id.salt.malang.fif.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class UserController {
	
	@GetMapping("/login")
	String login() {
		return "login";
	}
	
	@GetMapping("/logged-user-part")
	public String loggedUserPart()
	{
		return "logged-user-part";
	}
	
	@GetMapping("/users")
	public String ListUser()
	{
		return "users/user-list";
	}
}
