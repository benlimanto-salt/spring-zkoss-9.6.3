package id.salt.malang.fif.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import id.salt.malang.fif.service.ProductService;
import jakarta.servlet.http.HttpServletRequest;

@Controller
public class HomeController {
	
	@Autowired
	private ProductService service;
	
	public static ProductService productService;
	
	@GetMapping("/coba")
	public String ShowHome()
	{
		// Bad Hack, I still can't figure it out... ZKOSS sucks.. 
		if (HomeController.productService == null) HomeController.productService = service;
		
		return "index";
	}
	
	@GetMapping("/product/create")
	public String ShowCreate()
	{
		// Bad Hack, I still can't figure it out... ZKOSS sucks.. 
		if (HomeController.productService == null) HomeController.productService = service;
				
		return "createProduct";		
	}
	
	@GetMapping("/product")
	public String ShowProductList()
	{
		// Bad Hack, I still can't figure it out... ZKOSS sucks.. 
		if (HomeController.productService == null) HomeController.productService = service;
		
		return "listProduct";
	}
	
	@GetMapping("/product/edit")
	public String EditProductList()
	{
		// Bad Hack, I still can't figure it out... ZKOSS sucks.. 
		if (HomeController.productService == null) HomeController.productService = service;
		return "updateProduct";
	}
	
	@GetMapping("/product/edit/{id}")
	public String EditProductList(@PathVariable Integer id, HttpServletRequest request)
	{
		// Hacky? 
		request.getSession().setAttribute("id", id);
		return "updateProduct";
	}
	
	@GetMapping("/xhtml")
	public String XhtmlPage()
	{
		return "xhtml";
	}
	
	@GetMapping("/mvvm")
	public String MvvmPage()
	{
		return "mvvm";
	}
	
	@GetMapping("/mvvm/{id}")
	public String MvvmPageWithId(@PathVariable Integer id, HttpServletRequest request)
	{
		// Hacky? 
		request.getSession().setAttribute("id", id);
		return "mvvm";
	}
}
