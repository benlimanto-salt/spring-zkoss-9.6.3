package id.salt.malang.fif.zulViewModel;

import java.io.Serializable;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.ListModelList;

import id.salt.malang.fif.models.User;
import id.salt.malang.fif.service.UserService;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class ListUserViewModel implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private ListModelList<User> users = new ListModelList<User>();
	
	@WireVariable("userService")
	private UserService service;
	
	@Init
	public void init()
	{
		getUsers().clear();
		service.getList(null, null).forEach(u -> getUsers().add(u));
	}

	public ListModelList<User> getUsers() {
		return users;
	}

	public void setUsers(ListModelList<User> users) {
		this.users = users;
	}
	
	@Command
	public void editPage(@BindingParam String id)
	{
		Executions.sendRedirect("/users/edit/"+id);
	}
}
