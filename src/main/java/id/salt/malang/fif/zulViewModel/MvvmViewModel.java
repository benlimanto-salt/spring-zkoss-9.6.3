package id.salt.malang.fif.zulViewModel;

import java.io.Serializable;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Messagebox.Button;

import jakarta.servlet.http.HttpServletRequest;

public class MvvmViewModel implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String name = "";
	private String htmlBody = "";
	private Integer id;
	
	@Init
    public void init() {
        // MVVM Init
		// @see https://books.zkoss.org/zk-mvvm-book/8.0/viewmodel/initialization.html
		
		Execution exec = Executions.getCurrent();
		HttpServletRequest request = (HttpServletRequest) exec.getNativeRequest();
		
		Object o = request.getSession().getAttribute("id");
		
		if (o == null) return;
		
		String idSession = String.valueOf(o);
		request.getSession().removeAttribute("id");
		String idUrl = idSession;
		
		try {
			this.setId(Integer.parseInt(idUrl));
		}
		catch(Exception e)
		{
			Messagebox.show("Its not int : " + idUrl, new Button[] { Button.OK }, null);
		}
    }
	
	@Command
	@NotifyChange("*")
	public void Alerting()
	{
		Messagebox.show(getName());
	}
	
	@Command
	@NotifyChange("*")
	public void textChange()
	{
		this.htmlBody = this.name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHtmlBody() {
		return "<h4>" + name + "</h4>";
	}

	public void setHtmlBody(String htmlBody) {
		this.htmlBody = htmlBody;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
}
