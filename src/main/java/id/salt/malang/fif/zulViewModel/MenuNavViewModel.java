package id.salt.malang.fif.zulViewModel;

import java.io.Serializable;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.zk.ui.Executions;

public class MenuNavViewModel implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Command
	public void GoToUrl(@BindingParam("url") String url)
	{
		Executions.sendRedirect(url);
	}
}
