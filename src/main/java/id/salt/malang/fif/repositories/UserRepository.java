package id.salt.malang.fif.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import id.salt.malang.fif.models.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    
    List<User> findByFullNameLikeOrEmailLike(String name, String email);
}
